/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelos.Planeta;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;

/**
 *
 * @author Diego
 */
public class PlanetasControlador extends ActionSupport {
    
    private int id_planeta;
    private int id_estrella;
    private int id_tipo;
    private String nombre;
    private String simbologia;
    private double diametro_ecuatorial;
    private double masa;
    private double radio_orbital;
    private double periodo_orbital;
    private double periodo_rotacion;
    private String composicion_atmosfera;
    private String Imagen;
    private Planeta planeta;



    public int getId_planeta() {
        return id_planeta;
    }

    public void setId_planeta(int id_planeta) {
        this.id_planeta = id_planeta;
    }

    public int getId_estrella() {
        return id_estrella;
    }

    public void setId_estrella(int id_estrella) {
        this.id_estrella = id_estrella;
    }

    public int getId_tipo() {
        return id_tipo;
    }

    public void setId_tipo(int id_tipo) {
        this.id_tipo = id_tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSimbologia() {
        return simbologia;
    }

    public void setSimbologia(String simbologia) {
        this.simbologia = simbologia;
    }

    public double getDiametro_ecuatorial() {
        return diametro_ecuatorial;
    }

    public void setDiametro_ecuatorial(double diametro_ecuatorial) {
        this.diametro_ecuatorial = diametro_ecuatorial;
    }

    public double getMasa() {
        return masa;
    }

    public void setMasa(double masa) {
        this.masa = masa;
    }

    public double getRadio_orbital() {
        return radio_orbital;
    }

    public void setRadio_orbital(double radio_orbital) {
        this.radio_orbital = radio_orbital;
    }

    public double getPeriodo_orbital() {
        return periodo_orbital;
    }

    public void setPeriodo_orbital(double periodo_orbital) {
        this.periodo_orbital = periodo_orbital;
    }

    public double getPeriodo_rotacion() {
        return periodo_rotacion;
    }

    public void setPeriodo_rotacion(double periodo_rotacion) {
        this.periodo_rotacion = periodo_rotacion;
    }

    public String getComposicion_atmosfera() {
        return composicion_atmosfera;
    }

    public void setComposicion_atmosfera(String composicion_atmosfera) {
        this.composicion_atmosfera = composicion_atmosfera;
    }

    public String getImagen() {
        return Imagen;
    }

    public void setImagen(String Imagen) {
        this.Imagen = Imagen;
    }
    
    public Planeta getPlaneta(){
        
        return planeta;
    }

    
  /*  private Planeta P = new Planeta();
    
    public Planeta getJ() {
        return P;
    }

    public void setJ(Planeta j) {
        this.P = j;
    }
    
    //crear las acciones que le dimos al xml de struts (INICIO,JUGADOR,PARTIDO)
    
    public String execute() throws Exception{
     
      P.setNombre("Diego");
   
    return SUCCESS;
    }
    */
    @Override
   public String execute() throws Exception
        {
            planeta = new Planeta();
            planeta.setId_planeta(id_planeta);
            planeta.setId_estrella(id_estrella);
            planeta.setId_tipo(id_tipo);
            planeta.setNombre(nombre);
            planeta.setSimbologia(simbologia);
            planeta.setDiametro_ecuatorial(diametro_ecuatorial);
            planeta.setMasa(masa);
            planeta.setRadio_orbital(radio_orbital);
            planeta.setPeriodo_orbital(periodo_orbital);
            planeta.setPeriodo_rotacion(periodo_rotacion);
            planeta.setComposicion_atmosfera(composicion_atmosfera);
            planeta.setImagen(Imagen);


            return SUCCESS;
        }
}
