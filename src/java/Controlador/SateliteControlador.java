/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelos.Planeta;
import Modelos.Satelite;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;

/**
 *
 * @author Diego
 */
public class SateliteControlador extends ActionSupport {
    
    private int id_satelite;
    private String nombre;
    private String ubicacion;
    private Satelite satelite;

    
    

    public int getId_satelite() {
        return id_satelite;
    }

    public void setId_satelite(int id_satelite) {
        this.id_satelite = id_satelite;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
    public Satelite getSatelite(){
       
        return satelite;
        
    }
    
    @Override
   public String execute() throws Exception
        {
            satelite = new Satelite();
            satelite.setId_satelite(id_satelite);
            satelite.setNombre(nombre);
            satelite.setUbicacion(ubicacion);


            return SUCCESS;
        }
}
