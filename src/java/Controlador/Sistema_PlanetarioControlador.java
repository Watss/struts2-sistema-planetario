/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelos.Planeta;
import Modelos.SistemaPlanetario;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;

/**
 *
 * @author Diego
 */
public class Sistema_PlanetarioControlador extends ActionSupport {
    
    private int id_SP;
    private String nombre;
    private SistemaPlanetario sp;

    public SistemaPlanetario getSp() {
        return sp;
    }

    public void setId_SP(int id_SP) {
        this.id_SP = id_SP;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public int getId_SP() {
        return id_SP;
    }
    @Override
    public String execute() throws Exception
        {
            sp = new SistemaPlanetario();
            sp.setId_SP(id_SP);
            sp.setNombre(nombre);
            return SUCCESS;
        }
    
}
