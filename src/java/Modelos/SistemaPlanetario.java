/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author Diego
 */
public class SistemaPlanetario {
    
    private int id_SP;
    private String nombre;

    public int getId_SP() {
        return id_SP;
    }

    public String getNombre() {
        return nombre;
    }

    public void setId_SP(int id_SP) {
        this.id_SP = id_SP;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public SistemaPlanetario(int id_SP, String nombre) {
        this.id_SP = id_SP;
        this.nombre = nombre;
    }

    public SistemaPlanetario() {
    }
    
    
}
