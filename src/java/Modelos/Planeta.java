/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author Diego
 */
public class Planeta {
    
    private int id_planeta;
    private int id_estrella;
    private int id_tipo;
    private String nombre;
    private String simbologia;
    private double diametro_ecuatorial;
    private double masa;
    private double radio_orbital;
    private double periodo_orbital;
    private double periodo_rotacion;
    private String composicion_atmosfera;
    private String Imagen;

    public Planeta(int id_planeta, int id_estrella, int id_tipo, String nombre, String simbologia, double diametro_ecuatorial, double masa, double radio_orbital, double periodo_orbital, double periodo_rotacion, String composicion_atmosfera, String Imagen) {
        this.id_planeta = id_planeta;
        this.id_estrella = id_estrella;
        this.id_tipo = id_tipo;
        this.nombre = nombre;
        this.simbologia = simbologia;
        this.diametro_ecuatorial = diametro_ecuatorial;
        this.masa = masa;
        this.radio_orbital = radio_orbital;
        this.periodo_orbital = periodo_orbital;
        this.periodo_rotacion = periodo_rotacion;
        this.composicion_atmosfera = composicion_atmosfera;
        this.Imagen = Imagen;
    }

    public Planeta() {
    }

    public int getId_planeta() {
        return id_planeta;
    }

    public void setId_planeta(int id_planeta) {
        this.id_planeta = id_planeta;
    }

    public int getId_estrella() {
        return id_estrella;
    }

    public void setId_estrella(int id_estrella) {
        this.id_estrella = id_estrella;
    }

    public int getId_tipo() {
        return id_tipo;
    }

    public void setId_tipo(int id_tipo) {
        this.id_tipo = id_tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSimbologia() {
        return simbologia;
    }

    public void setSimbologia(String simbologia) {
        this.simbologia = simbologia;
    }

    public double getDiametro_ecuatorial() {
        return diametro_ecuatorial;
    }

    public void setDiametro_ecuatorial(double diametro_ecuatorial) {
        this.diametro_ecuatorial = diametro_ecuatorial;
    }

    public double getMasa() {
        return masa;
    }

    public void setMasa(double masa) {
        this.masa = masa;
    }

    public double getRadio_orbital() {
        return radio_orbital;
    }

    public void setRadio_orbital(double radio_orbital) {
        this.radio_orbital = radio_orbital;
    }

    public double getPeriodo_orbital() {
        return periodo_orbital;
    }

    public void setPeriodo_orbital(double periodo_orbital) {
        this.periodo_orbital = periodo_orbital;
    }

    public double getPeriodo_rotacion() {
        return periodo_rotacion;
    }

    public void setPeriodo_rotacion(double periodo_rotacion) {
        this.periodo_rotacion = periodo_rotacion;
    }

    public String getComposicion_atmosfera() {
        return composicion_atmosfera;
    }

    public void setComposicion_atmosfera(String composicion_atmosfera) {
        this.composicion_atmosfera = composicion_atmosfera;
    }

    public String getImagen() {
        return Imagen;
    }

    public void setImagen(String Imagen) {
        this.Imagen = Imagen;
    }
    
    
}
