/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author Diego
 */
public class Satelite {
    
    private int id_satelite;
    private String nombre;
    private String ubicacion;

    public Satelite(int id_satelite, String nombre, String ubicacion) {
        this.id_satelite = id_satelite;
        this.nombre = nombre;
        this.ubicacion = ubicacion;
    }

    public Satelite() {
    }
    

    public int getId_satelite() {
        return id_satelite;
    }

    public void setId_satelite(int id_satelite) {
        this.id_satelite = id_satelite;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
    
    
}
