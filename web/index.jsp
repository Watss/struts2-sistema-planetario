<%-- 
    Document   : index
    Created on : 29-10-2018, 17:05:38
    Author     : Diego
--%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
         <link rel="stylesheet" href="css/bootstrap.min.css">   		
        <script src="js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Diego Gonzalez</title>
    </head>
    <body>
        <div class="card text-center">
                <div class="card-header">
                    <h1 >Menú Sistema Planetario</h1>
                </div>
                <div class="card-body">
                  <h5 class="card-title">Navegar en el sistema</h5>
                  <p class="card-text">Pulse cualquiera de las opciones</p>
                  <div class="btn-group" role="group" aria-label="Basic example">
                    <a href="planeta"><button type="button" class="btn btn-secondary">Agregar Planeta</button></a>
                   <a href="estrella"> <button type="button" class="btn btn-secondary"> Agregar Estrella</button></a>
                    <a href="s_planetario"><button type="button" class="btn btn-secondary">Agregar Sistema Planetario</button></a>
                    <a href="satelite"><button type="button" class="btn btn-secondary">Agregar Satelite</button></a>
                  </div>
                </div>
                <div class="card-footer text-muted">
                  Diego Gonzalez®
                </div>
       </div>
        
    </body>
</html>
