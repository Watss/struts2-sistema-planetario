<%-- 
    Document   : SateliteMuestra
    Created on : 29-10-2018, 23:28:07
    Author     : Diego
--%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="css/bootstrap.min.css">   		
        <script src="js/bootstrap.min.js"></script>  
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Satelite Muestra</title>
    </head>
    <body>
        <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="text-center">Satelite Agregado</h3>
            </div>
            <div class="card-body">
              <table class="table table-dark">
                    <thead>
                      <tr>
                        <th scope="col">Nro Satelite</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Ubicacion</th>
                       
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row"><s:property value="satelite.id_satelite" /></th>
                        <td><s:property value="satelite.nombre" /></td>
                        <td><s:property value="satelite.ubicacion" /></td>
                        
                      </tr>
                      
                    </tbody>
                  </table>
              
            </div>
        </div>
         </div>
    </body>
</html>
