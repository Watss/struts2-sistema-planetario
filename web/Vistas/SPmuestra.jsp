<%-- 
    Document   : SPMuestra
    Created on : 29-10-2018, 23:40:19
    Author     : Diego
--%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="css/bootstrap.min.css">   		
        <script src="js/bootstrap.min.js"></script>  
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistema Planetario</title>
    </head>
    <body>
        <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="text-center">Sistema Planetario Agregado</h3>
            </div>
            <div class="card-body">
              <table class="table table-dark">
                    <thead>
                      <tr>
                        <th scope="col">Nro Sistema Planetario</th>
                        <th scope="col">Nombre</th>
                        
                       
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row"><s:property value="sp.id_SP" /></th>
                        <td><s:property value="sp.nombre" /></td>
                        
                        
                      </tr>
                      
                    </tbody>
                  </table>
              
            </div>
        </div>
         </div>
    </body>
</html>
