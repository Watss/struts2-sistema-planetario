<%-- 
    Document   : Satelite
    Created on : 29-10-2018, 18:17:52
    Author     : Diego
--%><%@ taglib prefix="s" uri="/struts-tags" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="css/bootstrap.min.css">   		
        <script src="js/bootstrap.min.js"></script>  
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Satelite</title>
    </head>
    <body>
       <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-center">Agregar Satelite</h3>
                </div>
                <div class="card-body">
                     
                <s:form action="addSatelite">
                    <div class="form-group">
                        
                        <s:textfield name="id_satelite" label="Id Satelite " class="form-control"/>
                        <s:textfield name="nombre" label="Nombre " class="form-control"/>
                        <s:textfield name="ubicacion" label="Ubicacion " class="form-control"/>
                        <s:submit value="Enviar" class="btn btn-success"/>
                    </div>
                </s:form>
                    
                </div>
            </div>
        </div>
    </body>
</html>
