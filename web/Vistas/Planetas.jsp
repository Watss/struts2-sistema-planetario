<%-- 
    Document   : Planetas
    Created on : 29-10-2018, 17:15:24
    Author     : Diego
--%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="css/bootstrap.min.css">   		
        <script src="js/bootstrap.min.js"></script>  
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Planetas</title>
    </head>
    <body>
        <h1>Mantenedor Planetas</h1>
        
        <s:form action="addPlanetas">
                <s:textfield name="id_planeta" label="id planeta" />
                <s:textfield name="id_estrella" label="id estrella" />
                <s:textfield name="id_tipo" label="id tipo" />
                <s:textfield name="nombre" label="Nombre" />
                <s:textfield name="simbologia" label="Simbologia" />
                <s:textfield name="diametro_ecuatorial" label="Diametro ecuatorial" />
                <s:textfield name="masa" label="Masa" />
                <s:textfield name="radio_orbital" label="Radio Orbital" />
                <s:textfield name="periodo_orbital" label="Periodo orbital" />
                <s:textfield name="periodo_rotacion" label="Periodo Rotacion" />
                <s:textfield name="composicion_atmosfera" label="Comp. Atmosfera" />
                <s:textfield name="imagen" label="url imagen" />
                <s:submit value="Enviar" />
        </s:form>
        
        
    </body>
</html>
