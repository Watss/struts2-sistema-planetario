<%-- 
    Document   : Estrellamuestra
    Created on : 29-10-2018, 18:38:13
    Author     : Diego
--%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/bootstrap.min.css">   		
        <script src="js/bootstrap.min.js"></script>   
        <title>Mostrar Estrella</title>
    </head>
    <body>
       
<div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="text-center">Estrella Agregada</h3>
            </div>
            <div class="card-body">
              <table class="table table-dark">
                    <thead>
                      <tr>
                        <th scope="col">Nro Estrella</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Nro Sistema Planetario</th>
                       
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row"><s:property value="estrella.id_estrella" /></th>
                        <td><s:property value="estrella.nombre" /></td>
                        <td><s:property value="estrella.id_sistema_planetario" /></td>
                        
                      </tr>
                      
                    </tbody>
                  </table>
              
            </div>
        </div>
         </div>
        
    </body>
</html>
