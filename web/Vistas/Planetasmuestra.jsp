<%-- 
    Document   : Planetasmuestra
    Created on : 29-10-2018, 18:03:25
    Author     : Diego
--%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="css/bootstrap.min.css">   		
        <script src="js/bootstrap.min.js"></script>  
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="text-center">Planeta Agregado</h3>
            </div>
            <div class="card-body">
              <table class="table table-dark">
                    <thead>
                      <tr>
                        <th scope="col">Nro Planeta</th>
                        <th scope="col">Nro Estrella</th>
                        <th scope="col">Nro Tipo</th>
                        <th scope="col">Nombre</th>
                        
                       
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row"><s:property value="planeta.id_planeta" /></th>
                        <td><s:property value="planeta.id_estrella" /></td>
                        <td><s:property value="planeta.id_tipo" /></td>
                        <td><s:property value="planeta.nombre" /></td>
                      </tr>
                      
                    </tbody>
                  </table>
              
            </div>
        </div>
         </div>
        <h1>Planeta Agregado</h1>
        Nombre: <strong><s:property value="planeta.nombre" /></strong> <br />
        Id Estrella <strong><s:property value="planeta.id_estrella" /></strong> <br />
        Id Tipo <strong><s:property value="planeta.id_tipo" /></strong> <br />
        Id Planeta: <strong><s:property value="planeta.id_planeta" /></strong>
    </body>
</html>
